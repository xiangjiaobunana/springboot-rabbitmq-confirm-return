package confirm;

import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class GetMsg1 {
    private static final String queue_name = "queue_xj_topic_1";
    private static final String exchange_name = "test_exchange_topics";
    public static void main(String[] args) throws Exception {
        Connection rabbitConn = ConnectUtils.getRabbitConn();

        // 创建通信管道
        Channel channel = rabbitConn.createChannel();

        /**
         * 定义队列;
         * 参数一：队列名称
         * 参数二：队列是否持久化
         * 参数三：是否私有
         * 参数四：是否自动删除
         * 参数五：设置队列类型
         */
        channel.queueDeclare(queue_name,true,false,false,null);
        // 将队列绑定至交换机中
        channel.queueBind(queue_name,exchange_name,"#.wuhan.#");

        // 获取并消费数据
        channel.basicConsume(queue_name,false,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                log.info("#.wuhan.#  识别并消费数据，数据id：{}，内容为：{}",envelope.getDeliveryTag(),new String(body));
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        });
    }
}
