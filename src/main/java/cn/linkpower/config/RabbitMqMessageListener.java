package cn.linkpower.config;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component  // 交给Spring管理
public class RabbitMqMessageListener {

    /**
     * 监听哪个队列  xiangjiao.queue
     */
    @RabbitListener(queues = MQConfiguration.QUEUQ_NAME)
    public void getXJQueueMsg(String msg, Channel channel, Message message) throws Exception {
        log.info("#################");
        log.info("消息队列  监听   收到  xiangjiao.queue 信息");
        log.info("msg:{}",msg);
        log.info("channel:{}",channel);
        log.info("message:{}",message);
        // 接收消息，但不批量处理
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        log.info("#################");
    }
}
